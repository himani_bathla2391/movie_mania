//
//  ViewControllerFirst.m
//  
//
//  Created by SAGAR THUKRAL on 27/11/15.
//
//

#import "ViewControllerFirst.h"
#import "ViewController.h"
@interface ViewControllerFirst ()

@end

@implementation ViewControllerFirst
@synthesize usernameLabel;
@synthesize passwordLabel;
@synthesize usernameTextfield;
@synthesize passwordTextfield;
@synthesize loginButton;
@synthesize imageView;
@synthesize movieMania;
ViewController* viewControllerObject;

- (void)viewDidLoad {
    [super viewDidLoad];
    [passwordTextfield setSecureTextEntry:YES];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)loginButtonAction:(id)sender {
    if ([usernameTextfield.text isEqualToString:@"clicklabs"] &&[passwordTextfield.text isEqualToString:@"12345"]) {
        
        
        UIStoryboard* mystoryboard =[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
     
        viewControllerObject= [mystoryboard instantiateViewControllerWithIdentifier:@"secondController"];
        
        [self.navigationController pushViewController:viewControllerObject animated:YES];
        
    }
    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Login Error" message:@"Incorrect Details" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [usernameTextfield resignFirstResponder];
    [passwordTextfield resignFirstResponder];
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
