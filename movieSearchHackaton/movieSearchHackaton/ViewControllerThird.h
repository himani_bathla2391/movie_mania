//
//  ViewControllerThird.h
//  
//
//  Created by SAGAR THUKRAL on 27/11/15.
//
//

#import <UIKit/UIKit.h>

@interface ViewControllerThird : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *image_backgroundThird;
@property (nonatomic,strong)NSMutableArray *destdata;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *releaseDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *actorsLabel;
@property (weak, nonatomic) IBOutlet UILabel *directorsLabel;
@property (weak, nonatomic) IBOutlet UILabel *writersLAbel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (weak, nonatomic) IBOutlet UILabel *runTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratedLabel;
@property (weak, nonatomic) IBOutlet UILabel *languageLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *okbutton;







@end
