//
//  ViewControllerThird.m
//  
//
//  Created by SAGAR THUKRAL on 27/11/15.
//
//

#import "ViewControllerThird.h"

@interface ViewControllerThird ()

@end

@implementation ViewControllerThird
@synthesize image_backgroundThird;
@synthesize destdata;
@synthesize titleLabel;
@synthesize releaseDateLabel;
@synthesize actorsLabel;
@synthesize directorsLabel;
@synthesize writersLAbel;
@synthesize genreLabel;
@synthesize ratedLabel;
@synthesize runTimeLabel;
@synthesize languageLabel;
@synthesize descriptionLabel;
@synthesize okbutton;
- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *showTitle=[destdata objectAtIndex:0];
    titleLabel.text=[NSString stringWithFormat:@"%@",showTitle];
    
    NSString *showDescription=[destdata objectAtIndex:1];
    descriptionLabel.text=[NSString stringWithFormat:@"%@",showDescription];
    
    NSString *showReleaseDate=[destdata objectAtIndex:2];
    releaseDateLabel.text=[NSString stringWithFormat:@"%@",showReleaseDate];
    
    NSString *actorsName=[destdata objectAtIndex:3];
    actorsLabel.text=[NSString stringWithFormat:@"%@",actorsName];
    
    NSString *showDirector=[destdata objectAtIndex:6];
    directorsLabel.text=[NSString stringWithFormat:@"%@",showDirector];
    
    NSString *showGenre=[destdata objectAtIndex:7];
    genreLabel.text=[NSString stringWithFormat:@"%@",showGenre];
    
    NSString *showLanguage=[destdata objectAtIndex:8];
    languageLabel.text=[NSString stringWithFormat:@"%@",showLanguage];
    
    NSString *showRunTime=[destdata objectAtIndex:9];
    runTimeLabel.text=[NSString stringWithFormat:@"%@",showRunTime];
    
    NSString *showWriter=[destdata objectAtIndex:10];
    writersLAbel.text=[NSString stringWithFormat:@"%@",showWriter];
    
    NSString *showRating=[destdata objectAtIndex:11];
    ratedLabel.text=[NSString stringWithFormat:@"%@",showRating];
    

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)okbuttonAction:(id)sender {
    [destdata removeAllObjects];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
