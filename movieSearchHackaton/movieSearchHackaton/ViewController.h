//
//  ViewController.h
//  movieSearchHackaton
//
//  Created by Click Labs134 on 11/27/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
@interface ViewController : UIViewController

<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *image_background;
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet UISearchBar *search;


@end

