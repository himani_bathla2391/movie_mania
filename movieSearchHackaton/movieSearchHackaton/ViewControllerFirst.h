//
//  ViewControllerFirst.h
//  
//
//  Created by SAGAR THUKRAL on 27/11/15.
//
//

#import <UIKit/UIKit.h>

@interface ViewControllerFirst : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *movieMania;

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
