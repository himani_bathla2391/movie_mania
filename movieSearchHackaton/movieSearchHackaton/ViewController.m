//
//  ViewController.m
//  movieSearchHackaton
//
//  Created by Click Labs134 on 11/27/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"
#import "ViewControllerFirst.h"
#import "ViewControllerThird.h"
NSMutableArray *result;
NSString *searchQuery;
NSString* searchText;
NSPredicate *resultPredicate;
NSString* data;
NSURLRequest *request;
NSURL *url;
NSMutableArray *showMovieTitleArray;
NSMutableArray *showMoviePicArray;
NSMutableArray *showMovieDetailsArray;
NSMutableArray *showMovieDescription;
NSMutableArray *showMovieReleaseDate;
NSMutableArray *destData;
int count;
ViewControllerThird* viewControllerThirdObject;
@interface ViewController ()

@end

@implementation ViewController
@synthesize table;
@synthesize search;
@synthesize image_background;
- (void)viewDidLoad {
    [super viewDidLoad];
    result=[[NSMutableArray alloc]init];
    showMovieTitleArray=[[NSMutableArray alloc]init];
    showMoviePicArray=[[NSMutableArray alloc]init];
    showMovieDescription=[[NSMutableArray alloc]init];
    showMovieReleaseDate=[[NSMutableArray alloc]init];
    showMovieDetailsArray=[[NSMutableArray alloc]init];
    [table reloadData];;
    table.hidden =YES;
    count=0;


    // Do any additional setup after loading the view, typically from a nib.
}


-(void) getApi
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSLog(@"%@",data);
    NSString *unescaped = @"http://www.omdbapi.com/?t=%@";
    NSString *escapedString = [unescaped stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    [manager GET:[NSString stringWithFormat:@"http://www.omdbapi.com/?t=%@",data]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id json)
     {
         if (json)
         {
             NSLog(@"%@",json);
             
             //title
             NSString *actorDictionary=json[@"Title"];
             [showMovieTitleArray addObject:actorDictionary];
             [showMovieDetailsArray addObject:actorDictionary];
             
             
             //poster
             NSString *imageDictionary=json[@"Poster"];
             NSString *picURL=[NSString stringWithFormat:@"%@",picURL];
             // NSLog(@"%@",imageDictionary);
             [showMoviePicArray addObject:picURL];
             //NSLog(@"%@",showMoviePicArray[0]);
             
             //description
             NSString *Description=json[@"Plot"];
             NSLog(@"%@",Description);
             NSString *details=[NSString stringWithFormat:@"%@",Description];
             [showMovieDescription addObject:details];
             [showMovieDetailsArray addObject:details];
             
             
             //release date
             NSString *releaseDate=json[@"Released"];
             [showMovieReleaseDate addObject:releaseDate];
             [showMovieDetailsArray addObject:releaseDate];
             
             //Actors
             NSString *actors=json[@"Actors"];
             NSString *actorsName=[NSString stringWithFormat:@"%@",actors];
             [showMovieDetailsArray addObject:actorsName];
             
             //Awards
             NSString *awards=json[@"Awards"];
             NSString *awardsName=[NSString stringWithFormat:@"%@",awards];
             [showMovieDetailsArray addObject:awardsName];
             
             //Country
             NSString *country=json[@"Country"];
             [showMovieDetailsArray addObject:country];
             
             //Director
             NSString *director=json[@"Director"];
             [showMovieDetailsArray addObject:director];
             
             //Genre
             NSString *genre=json[@"Language"];
             NSString *genreName=[NSString stringWithFormat:@"%@",genre];
             [showMovieDetailsArray addObject:genreName];
             
             //Language
             NSString *language=json[@"Genre"];
             NSString *languagesName=[NSString stringWithFormat:@"%@",genre];
             [showMovieDetailsArray addObject:languagesName];
             
             //rated
             NSString *rated=json[@"Rated"];
             NSString *ratedName=[NSString stringWithFormat:@"%@",rated];
             [showMovieDetailsArray addObject:ratedName];
             
             //Writer
             NSString *writer=json[@"Director"];
             [showMovieDetailsArray addObject:writer];
             
             //rating
             NSNumber *rating=json[@"imdbRating"];
             [showMovieDetailsArray addObject:rating];
             
             [table reloadData];
         }
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error.description);
             NSLog(@"ERROR: %@ \n\n 1. ",error.localizedDescription);
             
             
             
         }];
    
    
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{if(count < 10){
    table.hidden = NO;
    data=search.text;
    [ self getApi];
    count++;
}
else{
    UIAlertView *alert= [[UIAlertView alloc]initWithTitle:@"Subscription required" message:@"Searched more than 10 times.Make a paid account" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return showMovieTitleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableView* cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        cell.backgroundColor=[UIColor lightGrayColor];
    
    UILabel *titleLabel=(UILabel *)[cell viewWithTag:100];
    titleLabel.text =showMovieTitleArray[indexPath.row];
    
    //UIImageView *moviePoster=(UIImageView *)[cell viewWithTag:101];
    //moviePoster.image = [UIImage imageNamed:showMoviePicArray[indexPath.row]];
    
    UILabel *releaseDateLabel=(UILabel *)[cell viewWithTag:102];
    releaseDateLabel.text =showMovieReleaseDate[indexPath.row];

    return cell;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if([search.text isEqualToString: @""])
    {
        [search resignFirstResponder];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIStoryboard* mystoryboard =[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    viewControllerThirdObject= [mystoryboard instantiateViewControllerWithIdentifier:@"thirdController"];
    
    [self.navigationController pushViewController:viewControllerThirdObject animated:YES];
     viewControllerThirdObject.destdata=showMovieDetailsArray;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
